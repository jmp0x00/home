#!/bin/bash

file_patch=gnuscreen-21653.patch

wget "http://lists.gnu.org/archive/html/screen-devel/2011-05/bin4fE4KxGNNI.bin" -O $file_patch
git clone "git://git.savannah.gnu.org/screen.git"
mv $file_patch screen/
cd screen/
patch -p 1 < $file_patch
cd src/
./autogen.sh
./configure --enable-colors256
make
make install
cd ../../
