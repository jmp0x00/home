syntax on
colorscheme jmp0x00

set number
set cursorline
set autoindent
set smartindent
set ai
set showmatch
set tabstop=4
set shiftwidth=4
set t_Co=256
set wildmenu
set autochdir
set hlsearch " colored search
set incsearch
set nocompatible " not vi
set backspace=2
"set spell spelllang=ru
set termencoding=utf-8
set fileencodings=utf8,cp1251
set encoding=utf8
"Syntastic
let g:syntastic_enable_signs=1

" For vundle
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
filetype plugin indent on

"vim/scripts
Bundle 'Syntastic'
Bundle 'taglist.vim'
Bundle 'vundle'
