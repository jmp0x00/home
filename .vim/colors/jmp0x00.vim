
" Vim color file jmp0x00

set background=dark
" hi clear
if exists("syntax_on")
	syntax reset
endif
let g:colors_name = "jmp0x00"


hi Cursor		gui=NONE	guibg=#f8f8f0	guifg=NONE		cterm=NONE	ctermbg=231	ctermfg=NONE
"hi Visual		gui=NONE	guibg=#161a1f	guifg=NONE		cterm=NONE	ctermbg=16	ctermfg=NONE
hi Visual		gui=NONE	guibg=#161a1f	guifg=NONE		cterm=NONE	ctermbg=238	ctermfg=NONE
hi CursorLine	gui=NONE	guibg=#434443	guifg=NONE	cterm=NONE	ctermbg=237	ctermfg=NONE
hi CursorColumn	gui=NONE	guibg=#434443	guifg=NONE	cterm=NONE	ctermbg=237	ctermfg=NONE
hi ColorColumn	gui=NONE	guibg=#434443	guifg=NONE	cterm=NONE	ctermbg=237	ctermfg=NONE
hi LineNr		gui=NONE	guibg=#434443	guifg=#939491	cterm=NONE	ctermbg=237	ctermfg=246
hi VertSplit	gui=NONE	guibg=#696a68	guifg=#696a68	cterm=NONE	ctermbg=242	ctermfg=242
hi MatchParen	gui=NONE	guibg=NONE	guifg=#c8d7e8	cterm=NONE	ctermbg=NONE	ctermfg=188
hi StatusLine	gui=NONE	guibg=#696a68	guifg=#f8f8f2	cterm=NONE	ctermbg=242	ctermfg=231
hi StatusLineNC	gui=NONE	guibg=#696a68	guifg=#f8f8f2	cterm=NONE	ctermbg=242	ctermfg=231
hi Pmenu		gui=NONE	guibg=NONE	guifg=NONE	cterm=NONE	ctermbg=NONE	ctermfg=NONE
hi PmenuSel		gui=NONE	guibg=#161a1f	guifg=NONE	cterm=NONE	ctermbg=16	ctermfg=NONE
hi IncSearch	gui=NONE	guibg=#61676d	guifg=NONE	cterm=NONE	ctermbg=59	ctermfg=236
hi Search		gui=NONE	guibg=#61676d	guifg=NONE	cterm=NONE	ctermbg=59	ctermfg=236
hi Directory	gui=NONE	guibg=NONE	guifg=#8fbe00	cterm=NONE	ctermbg=NONE	ctermfg=106
hi Folded		gui=NONE	guibg=#2f3030	guifg=#556270	cterm=NONE	ctermbg=236	ctermfg=59

hi Normal		gui=NONE	guibg=#2f3030	guifg=#f8f8f2	cterm=NONE	ctermbg=236	ctermfg=231
"hi Boolean		gui=NONE	guibg=NONE	guifg=#8fbe00	cterm=NONE	ctermbg=NONE	ctermfg=106
"hi Character	gui=NONE	guibg=NONE	guifg=#8fbe00	cterm=NONE	ctermbg=NONE	ctermfg=106
hi Comment		gui=NONE	guibg=NONE	guifg=#556270	cterm=NONE	ctermbg=NONE	ctermfg=59
"hi Conditional	gui=NONE	guibg=NONE	guifg=#c8d7e8	cterm=NONE	ctermbg=NONE	ctermfg=188
hi Constant		gui=NONE	guibg=NONE	guifg=#8fbe00	cterm=NONE	ctermbg=NONE	ctermfg=149
"hi Define		gui=NONE	guibg=NONE	guifg=#c8d7e8	cterm=NONE	ctermbg=NONE	ctermfg=188
hi ErrorMsg		gui=NONE	guibg=#ff0000	guifg=#f8f8f0	cterm=NONE	ctermbg=196	ctermfg=0
hi WarningMsg	gui=NONE	guibg=#ffff00	guifg=#f8f8f0	cterm=NONE	ctermbg=226	ctermfg=0
"hi Float		gui=NONE	guibg=NONE	guifg=#8fbe00	cterm=NONE	ctermbg=NONE	ctermfg=106
"hi Function		gui=NONE	guibg=NONE	guifg=#aee239	cterm=NONE	ctermbg=NONE	ctermfg=149
"hi Identifier	gui=NONE	guibg=NONE	guifg=#4ecdc4	cterm=NONE	ctermbg=NONE	ctermfg=80
hi Identifier	gui=NONE	guibg=NONE	guifg=#aee239	cterm=NONE	ctermbg=NONE	ctermfg=106
"hi Keyword		gui=NONE	guibg=NONE	guifg=#c8d7e8	cterm=NONE	ctermbg=NONE	ctermfg=188
"hi Label		gui=NONE	guibg=NONE	guifg=#e9ee00	cterm=NONE	ctermbg=NONE	ctermfg=190
"hi NonText		gui=NONE	guibg=#393a3a	guifg=#3b3a32	cterm=NONE	ctermbg=237	ctermfg=59
"hi Number		gui=NONE	guibg=NONE	guifg=#8fbe00	cterm=NONE	ctermbg=NONE	ctermfg=106
hi Operator		gui=NONE	guibg=NONE	guifg=#f8f8f2	cterm=NONE	ctermbg=NONE	ctermfg=231
"hi PreProc		gui=NONE	guibg=NONE	guifg=#c8d7e8	cterm=NONE	ctermbg=NONE	ctermfg=188
hi PreProc		gui=NONE	guibg=NONE	guifg=#4ecdc4	cterm=NONE	ctermbg=NONE	ctermfg=80
hi Special		gui=NONE	guibg=NONE	guifg=#f8f8f2	cterm=NONE	ctermbg=NONE	ctermfg=231
"hi SpecialKey	gui=NONE	guibg=#434443	guifg=#3b3a32	cterm=NONE	ctermbg=238	ctermfg=59
"hi Statement	gui=NONE	guibg=NONE	guifg=#c8d7e8	cterm=NONE	ctermbg=NONE	ctermfg=188
hi Statement	gui=NONE	guibg=NONE	guifg=#00a8c6	cterm=NONE	ctermbg=NONE	ctermfg=38
"hi StorageClass	gui=NONE	guibg=NONE	guifg=#4ecdc4	cterm=NONE	ctermbg=NONE	ctermfg=80
"hi String		gui=NONE	guibg=NONE	guifg=#e9ee00	cterm=NONE	ctermbg=NONE	ctermfg=190
"hi Tag			gui=NONE	guibg=NONE	guifg=#00a8c6	cterm=NONE	ctermbg=NONE	ctermfg=38
"hi Title		gui=NONE	guibg=NONE	guifg=#f8f8f2	cterm=NONE	ctermbg=NONE	ctermfg=231
"hi Todo			gui=NONE 	guibg=NONE 	guifg=#556270 	cterm=NONE	ctermbg=NONE 	ctermfg=59	 	 
"hi Type			gui=NONE	guibg=NONE	guifg=NONE	cterm=NONE	ctermbg=NONE	ctermfg=NONE
"hi Type			gui=NONE	guibg=NONE	guifg=#4ecdc4	cterm=NONE	ctermbg=NONE	ctermfg=80
hi Type			gui=NONE	guibg=NONE	guifg=#00a8c6	cterm=NONE	ctermbg=NONE	ctermfg=38
"hi Underlined	gui=NONE	guibg=NONE	guifg=NONE	cterm=NONE	ctermbg=NONE	ctermfg=NONE
hi MatchParen	gui=NONE	guibg=NONE	guifg=#00a8c6	cterm=NONE 	ctermbg=80		ctermfg=231
